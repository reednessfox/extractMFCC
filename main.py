import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
# PRE-PROCESSING
# ====================================================================================
#  transcoding of input audiofiles:
#   sample rate - 32000 Hz
#   channels    - mono (selection of the 1st channel)

from pydub import AudioSegment as AS

def transcoding_audiofiles(name):
    sound = AS.from_file(name, format='wav')
    sound = sound.set_frame_rate(32000)
    sound = sound.set_channels(1)
    sound.export(name, format='wav')
    
    num_of_channels = sound.channels
    sample_width = sound.sample_width
    frame_rate = sound.frame_rate
    
    print('\nsample_width:', sample_width*8, 'bit')
    print('num_of_channels:', num_of_channels)
    print('frame_rate:', frame_rate)

# ====================================================================================
#  filtering with webRTC VAD
#   frame duration - 20 ms
#   vad filtering  - 3

import webrtcvad
from scipy.io import wavfile
import numpy as np
import librosa

def filter_VAD(filename, frame_duration, frame_length):
    sample_rate, data = wavfile.read(filename)
    vad = webrtcvad.Vad(3)
    data = np.pad(data, int(frame_length // 2), mode='reflect')
    frames = librosa.util.frame(data, frame_length, hop_length=frame_length // 4)

    frame_seq = []

    for i in range(frames.shape[1]):
        if(vad.is_speech(frames[:,i].tobytes(), sample_rate)):
            frame_seq.append(i)

    voiced_frames = np.array(frame_seq)
    print('frames shape:', frames.shape, 
          'voiced_frames shape:', voiced_frames.shape)
    
    return(voiced_frames)

# ====================================================================================
# EXTRACT FEATURES
# ====================================================================================
#  13 mfcc, 13 delta-mfcc, 13 delta-delta-mfcc (all: 39 features)

def extract_features(y, sample_rate, frame_duration, frame_length):
    y = np.float32( (y / 32768) )
    frame_length = int(sample_rate * frame_duration / 1000)
    S = librosa.feature.melspectrogram(y, sr=sample_rate, n_mels=128,
                                       fmax=8000, n_fft=frame_length,
                                       hop_length=frame_length // 4)
    log_S = librosa.core.power_to_db(S)
    mfcc = librosa.feature.mfcc(S=log_S, n_mfcc=13)
    delta_mfcc  = librosa.feature.delta(mfcc)
    delta2_mfcc = librosa.feature.delta(mfcc, order=2)
    
    print('mfcc shape:', mfcc.T.shape,
          'delta-mfcc shape:', delta_mfcc.T.shape,
          'delta2-mfcc shape:', delta_mfcc.T.shape)
    
    return(mfcc.T, delta_mfcc.T, delta2_mfcc.T)

# ====================================================================================
# GET ATTRIBUTES
# ====================================================================================
#  get attributes of wav files from EMOVO corpus

#   emotions (emo):        speakers (spkrs):    phrases(phrs):
#   0 anger                7  male 1            13 b1   20 l3
#   1 disgust              8  male 2            14 b2   21 l4
#   2 fear                 9  male 3            15 b3   22 n1
#   3 joy                  10 female 1          16 d1   23 n2
#   4 neural               11 female 2          17 d2   24 n3
#   5 sadness              12 female 3          18 l1   25 n4
#   6 surprise

emotions = { 'rab': 0, 'dis': 1, 'pau': 2,
             'gio': 3, 'neu': 4, 'tri': 5, 'sor': 6 }
speakers = { 'm1': 7, 'm2': 8, 'm3': 9,
             'f1': 10, 'f2': 11, 'f3': 12 }
phrases = { 'b1': 13, 'b2': 14, 'b3': 15, 'd1': 16, 'd2': 17,
            'l1': 18, 'l2': 19, 'l3': 20, 'l4': 21, 'n1': 22,
            'n2': 23, 'n3': 24, 'n4': 25, 'n5': 26 }

def get_attr(filename):
    emo  = filename[25:28]
    spkr = filename[29:31]
    phrs = filename[32:34]

    for i in emotions:
        if(emo == i):
            emo = emotions[i]

    for i in speakers:
        if(spkr == i):
            spkr = speakers[i]

    for i in phrases:
        if(phrs == i):
            phrs = phrases[i]

    print('attributes have been extracted') 
    
    return(emo, spkr, phrs)

# ====================================================================================
#  get all data: features + attributes

#   | 0-12 | 13-25 |  26-38 |   39    |   40    |   41   |
#   ------------------------------------------------------
#   | mfcc | delta | delta2 | emotion | speaker | phrase |

def get_all_data(filename, voiced_frames, mfcc, delta_mfcc, delta2_mfcc):
    
    frstM = mfcc[voiced_frames[0],:]
    scndM = mfcc[voiced_frames[1],:]
    tmpM = np.vstack((frstM, scndM))
    
    frstMD = delta_mfcc[voiced_frames[0],:]
    scndMD = delta_mfcc[voiced_frames[1],:]
    tmpMD = np.vstack((frstMD, scndMD))
    
    frstM2D = delta2_mfcc[voiced_frames[0],:]
    scndM2D = delta2_mfcc[voiced_frames[1],:]
    tmpM2D = np.vstack((frstM2D, scndM2D))
    
    for i in range(2, voiced_frames.shape[0]):
        scndM = mfcc[voiced_frames[i],:]
        tmpM = np.vstack((tmpM, scndM))
        
        scndMD = delta_mfcc[voiced_frames[i],:]
        tmpMD = np.vstack((tmpMD, scndMD))
        
        scndM2D = delta2_mfcc[voiced_frames[i],:]
        tmpM2D = np.vstack((tmpM2D, scndM2D))

    features = np.hstack((tmpM, tmpMD, tmpM2D))
    emo, spkr, phrs = get_attr(filename)
    
    emo = np.repeat(emo, features.shape[0])
    spkr = np.repeat(spkr, features.shape[0])
    phrs = np.repeat(phrs, features.shape[0])
     
    attributes = np.vstack((emo, spkr, phrs))
    attributes = attributes.T
    
    print('matrix with features shape:', np.hstack((features, attributes)).shape)
    
    return(np.hstack((features, attributes)), emo[0])

# ====================================================================================
# MAIN
# ====================================================================================

import glob

rab = np.array([[]])
dis = np.array([[]])
pau = np.array([[]])
gio = np.array([[]])
neu = np.array([[]])
tri = np.array([[]])
sor = np.array([[]])

for filename in glob.glob('emovo-db/wav/*.wav'):
    transcoding_audiofiles(filename)
    sample_rate, data = wavfile.read(filename)
    
    frame_duration = 20
    frame_length = int(sample_rate * frame_duration / 1000)
    
    voiced_frames = filter_VAD(filename, frame_duration, frame_length)
    
    mfcc, delta_mfcc, delta2_mfcc = extract_features(data, sample_rate, frame_duration,
                                                     frame_length)
    data, _emo = get_all_data(filename, voiced_frames, mfcc, delta_mfcc, delta2_mfcc)
    
    print('matrix with features shape 2:', data.shape)
    
    if _emo == 0:
        if rab.shape[1] == 0: rab = data
        else: rab = np.vstack((rab, data))
    elif _emo == 1:
        if dis.shape[1] == 0: dis = data
        else: dis = np.vstack((dis, data))
    elif _emo == 2:
        if pau.shape[1] == 0: pau = data
        else: pau = np.vstack((pau, data))
    elif _emo == 3:
        if gio.shape[1] == 0: gio = data
        else: gio = np.vstack((gio, data))
    elif _emo == 4:
        if neu.shape[1] == 0: neu = data
        else: neu = np.vstack((neu, data))
    elif _emo == 5:
        if tri.shape[1] == 0: tri = data
        else: tri = np.vstack((tri, data))
    elif _emo == 6:
        if sor.shape[1] == 0: sor = data
        else: sor = np.vstack((sor, data))

print('\n\nrab shape:', rab.shape, '\ndis shape:', dis.shape,
      '\npau shape:', pau.shape, '\ngio shape:', gio.shape,
      '\nneu shape:', neu.shape, '\ntri shape:', tri.shape, '\nsor shape:', sor.shape)

# ====================================================================================
# WRITE TO HDF5
# ====================================================================================

import h5py

file = h5py.File('emovodb.hdf5', 'w')

grp = file.create_group('features_emo')

dataset0 = grp.create_dataset('disgust', data = dis)
dataset1 = grp.create_dataset('joy', data = gio)
dataset2 = grp.create_dataset('fear', data = pau)
dataset3 = grp.create_dataset('anger', data = rab)
dataset4 = grp.create_dataset('surprise', data = sor)
dataset5 = grp.create_dataset('sadness', data = tri)
dataset6 = grp.create_dataset('neural', data = neu)

file.close()
