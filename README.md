# emotion-recognition-extract-MFCC
This is a small university project (course work #1) on the recognition of the emotions of a speaker based on the MFCC. Trained models - GMM.

The project was written in Jupyter Notebook.
